package com.alikosesakal.mooviettin.events;

import com.alikosesakal.mooviettin.models.TvShow;

/**
 * Created by ali on 16.12.2016.
 */

public class AddRemoveFollowingEvent {
    private TvShow tvShow;
    private boolean isAdd;

    public AddRemoveFollowingEvent(TvShow tvShow, boolean isAdd) {
        this.tvShow = tvShow;
        this.isAdd = isAdd;
    }

    public TvShow getTvShow() {
        return tvShow;
    }

    public void setTvShow(TvShow tvShow) {
        this.tvShow = tvShow;
    }

    public boolean isAdd() {
        return isAdd;
    }

    public void setAdd(boolean add) {
        isAdd = add;
    }
}
