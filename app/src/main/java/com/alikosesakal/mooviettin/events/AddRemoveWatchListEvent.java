package com.alikosesakal.mooviettin.events;

import com.alikosesakal.mooviettin.models.Movie;

/**
 * Created by ali on 14.12.2016.
 */

public class AddRemoveWatchListEvent {
    Movie movie;
    boolean isAdd;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public boolean isAdd() {
        return isAdd;
    }

    public void setAdd(boolean add) {
        isAdd = add;
    }

    public AddRemoveWatchListEvent(Movie movie, boolean isAdd) {
        this.movie = movie;
        this.isAdd = isAdd;
    }
}
