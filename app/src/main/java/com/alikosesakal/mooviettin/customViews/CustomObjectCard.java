package com.alikosesakal.mooviettin.customViews;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.events.AddRemoveFollowingEvent;
import com.alikosesakal.mooviettin.events.AddRemoveWatchListEvent;
import com.alikosesakal.mooviettin.models.Movie;
import com.alikosesakal.mooviettin.models.TvShow;
import com.alikosesakal.mooviettin.util.AppController;
import com.alikosesakal.mooviettin.util.ProfileHelper;
import com.alikosesakal.mooviettin.util.RequestGenerator;


/**
 * Created by ali on 23.11.2016.
 */

public class CustomObjectCard extends CardView {
    private static final int RELEASE_DATE_LENGTH = 4;
    private static final String DEFAULT_DATE = "1999";
    private static final int STAR_ICON_ELEVATION = 5;
    private static String releaseDate;
    private static String voteAvarage;
    private FrameLayout cardView;
    private TextView year, ratio;
    private ImageView objectImage, star;
    private Context mContext;
    private Movie movie;
    private TvShow tvShow;
    private ProgressBar progressBar;
    private String TAG = this.getClass().getSimpleName();


    public CustomObjectCard(Context mContext, AttributeSet mAttributeSet) {
        super(mContext, mAttributeSet);
        this.mContext = mContext;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.custom_movie_view, this, true);
        }
        LinearLayout infoLayout = findViewById(R.id.movie_info_layout);
        cardView = findViewById(R.id.card_view);
        progressBar = findViewById(R.id.movie_image_progress);
        objectImage = findViewById(R.id.movie_image);
        year = findViewById(R.id.year_textView);
        ratio = findViewById(R.id.ratio_textView);
        star = findViewById(R.id.star);
        ViewCompat.setElevation(star, STAR_ICON_ELEVATION);
        infoLayout.setMinimumHeight(star.getHeight());
    }


    public void setObject(final Object object, final OnItemObjectClickListener onItemClickListener, int typeID, int fragmentID) {
        if (typeID == AppController.FRAGMENT_MOVIES_ID || fragmentID == AppController.FRAGMENT_WATCH_LIST) {
            Log.e(TAG, "typeID == >>" + typeID + "    fragmentID ==>>" + fragmentID);
            this.movie = (Movie) object;
            String imagePath = RequestGenerator.GeneratePosterPath(movie.getPosterPath());
            progressBar.setVisibility(VISIBLE);
            progressBar.setIndeterminate(true);

            Glide.with(mContext).load(imagePath).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    progressBar.setIndeterminate(false);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                               boolean isFromMemoryCache, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    progressBar.setIndeterminate(false);

                    return false;
                }
            }).into(objectImage);

            if (movie.getReleaseDate() != null) {
                releaseDate = movie.getReleaseDate().substring(0, RELEASE_DATE_LENGTH);
            } else {
                releaseDate = "";
            }

            year.setText(releaseDate);
            if (movie.getVoteAverage().length() > 3) {
                voteAvarage = movie.getVoteAverage().substring(0, 3);
            } else {
                voteAvarage = movie.getVoteAverage();
            }

            star.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((Movie) object).isFavorited()) {
                        ((Movie) object).setFavorited(!((Movie) object).isFavorited());
                        star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.blueGrey200));
                        ProfileHelper.removeFromWatchList(((Movie) object));
                        AppController.getEventBus().post(new AddRemoveWatchListEvent(((Movie) object), false));
                    } else {
                        ((Movie) object).setFavorited(!((Movie) object).isFavorited());
                        star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.orange700));
                        ProfileHelper.addToWatchList(((Movie) object));
                        AppController.getEventBus().post(new AddRemoveWatchListEvent(((Movie) object), true));
                    }

                }
            });
            if (((Movie) object).isFavorited()) {
                star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.orange700));
            } else {
                star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.blueGrey200));
            }


            objectImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.OnObjectClick(object);
                }
            });

            ratio.setText(voteAvarage);
        } else if (typeID == AppController.FRAGMENT_TV_SHOWS_ID || fragmentID == AppController.FRAGMENT_FOLLOWING) {
            this.tvShow = (TvShow) object;
            String imagePath = RequestGenerator.GeneratePosterPath(tvShow.getPosterPath());
            progressBar.setVisibility(VISIBLE);
            progressBar.setIndeterminate(true);

            Glide.with(mContext).load(imagePath).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    progressBar.setIndeterminate(false);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                               boolean isFromMemoryCache, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    progressBar.setIndeterminate(false);
                    return false;
                }
            }).into(objectImage);

            if (tvShow.getFirstAirDate() != null && !tvShow.getFirstAirDate().equals("")) {
                releaseDate = tvShow.getFirstAirDate().substring(0, RELEASE_DATE_LENGTH);
            } else {
                releaseDate = DEFAULT_DATE;
            }


            star.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((TvShow) object).isFavorite()) {
                        ((TvShow) object).setFavorite(!((TvShow) object).isFavorite());
                        star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.blueGrey200));
                        ProfileHelper.removeFromFollowing(((TvShow) object));
                        AppController.getEventBus().post(new AddRemoveFollowingEvent(((TvShow) object), false));
                    } else {
                        ((TvShow) object).setFavorite(!((TvShow) object).isFavorite());
                        star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.orange700));
                        ProfileHelper.addToFollowing(((TvShow) object));
                        AppController.getEventBus().post(new AddRemoveFollowingEvent(((TvShow) object), true));
                    }
                }
            });

            if (((TvShow) object).isFavorite()) {
                star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.orange700));
            } else {
                star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.blueGrey200));
            }

            year.setText(releaseDate);
            voteAvarage = tvShow.getVoteAverage();
            if (voteAvarage.length() > 3) {
                voteAvarage = tvShow.getVoteAverage().substring(0, 3);
            }
            objectImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.OnObjectClick(tvShow);
                }
            });

            ratio.setText(voteAvarage);
        }
    }


    public Movie getMovie() {
        return movie;
    }


    public interface OnItemObjectClickListener {
        void OnObjectClick(Object object);
    }

}
