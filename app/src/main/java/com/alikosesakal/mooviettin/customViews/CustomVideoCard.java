package com.alikosesakal.mooviettin.customViews;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.models.Video;
import com.alikosesakal.mooviettin.util.RequestGenerator;

/**
 * Created by ali on 30.11.2016.
 */

public class CustomVideoCard extends CardView {
    private Video video;
    private Context mContext;
    private ImageView thumbnail;
    private ImageButton playButton;
    private AttributeSet attrs;
    OnItemVideoClickListener onItemClickListener;

    public CustomVideoCard(Context mContext, AttributeSet attrs) {
        super(mContext, attrs);
        this.mContext = mContext;
        this.attrs = attrs;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_video_card, this, true);

        thumbnail = (ImageView) findViewById(R.id.thumbnail_image);
        playButton = (ImageButton) findViewById(R.id.play_image_button);

    }

    public void setVideo(final Video video, final OnItemVideoClickListener onItemClickListener) {
        this.video = video;
        Log.e("CustomVideo", "Thumbnail ===>>" + RequestGenerator.GenerateYouTubeThumbnail(video.getKey()).get(0));
        Glide.with(mContext).load(RequestGenerator.GenerateYouTubeThumbnail(video.getKey()).get(0))
                .crossFade().into(thumbnail);

        playButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.OnVideoClick(video);
            }
        });

    }

    public CustomVideoCard(Context mContext) {
        super(mContext);
    }

    public CustomVideoCard(Context mContext, AttributeSet attrs, int defStyleAttr) {
        super(mContext, attrs, defStyleAttr);
    }

    public Video getVideo() {
        return video;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public ImageView getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ImageView thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ImageButton getPlayButton() {
        return playButton;
    }

    public void setPlayButton(ImageButton playButton) {
        this.playButton = playButton;
    }

    public AttributeSet getAttrs() {
        return attrs;
    }

    public void setAttrs(AttributeSet attrs) {
        this.attrs = attrs;
    }

    public OnItemVideoClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemVideoClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemVideoClickListener {
        public void OnVideoClick(Video video);
    }
}
