package com.alikosesakal.mooviettin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.adapters.ImageSliderAdapter;
import com.alikosesakal.mooviettin.adapters.ObjectCastAdapter;
import com.alikosesakal.mooviettin.adapters.SimilarMoviesAdapter;
import com.alikosesakal.mooviettin.adapters.TrailersRecyclerViewAdapter;
import com.alikosesakal.mooviettin.customViews.CustomObjectCard;
import com.alikosesakal.mooviettin.customViews.CustomVideoCard;
import com.alikosesakal.mooviettin.events.AddRemoveFollowingEvent;
import com.alikosesakal.mooviettin.models.IMDb;
import com.alikosesakal.mooviettin.models.Image;
import com.alikosesakal.mooviettin.models.RootCreditsObject;
import com.alikosesakal.mooviettin.models.RootImageJsonObject;
import com.alikosesakal.mooviettin.models.RootTvShowJsonObject;
import com.alikosesakal.mooviettin.models.RootVideoObject;
import com.alikosesakal.mooviettin.models.TvShow;
import com.alikosesakal.mooviettin.models.Video;
import com.alikosesakal.mooviettin.util.AppController;
import com.alikosesakal.mooviettin.util.ProfileHelper;
import com.alikosesakal.mooviettin.util.RequestGenerator;
import com.alikosesakal.mooviettin.util.response.AdapterBase;
import com.alikosesakal.mooviettin.util.response.ServiceCallback;
import com.alikosesakal.mooviettin.util.response.ServiceException;
import com.alikosesakal.mooviettin.util.response.ServiceOperations;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ActivityTvShowDetails extends AppCompatActivity implements AdapterBase.OnLoadListener
        , CustomVideoCard.OnItemVideoClickListener, CustomObjectCard.OnItemObjectClickListener {

    private String TAG = this.getClass().getSimpleName();
    /*************************
     * ADAPTERS
     ************************/
    private ImageSliderAdapter pagerAdapter;
    private TrailersRecyclerViewAdapter thumbnailAdapter;
    private SimilarMoviesAdapter similarTvShowsAdapter;
    private ObjectCastAdapter tvShowCastAdapter;

    /*************************
     * UI
     ************************/
    private TextView overview;
    private ViewPager mViewPager;
    private RecyclerView videosRecyclerView, similarTvShowsRecycler, tvShowCastRecycler;
    private LinearLayout scoreLayout, identityLayout;
    private ImageButton star;
    private CardView videoCard;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    /*************************
     * OBJECTS
     ************************/
    private ArrayList<Object> similarTvShowsList = new ArrayList<>();
    private ArrayList<Object> videos = new ArrayList<>();
    private ArrayList<Object> cast = new ArrayList<>();
    private int currentPage;
    private String tvShowID;
    private TvShow tvShow;
    private IMDb imdb;
    private LayoutInflater inflater = (LayoutInflater) AppController.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_show_details);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("Bundle");
        tvShowID = bundle.getString("TvShowID");

        Toolbar mToolBar = findViewById(R.id.tv_show_details_toolbar);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        setSupportActionBar(mToolBar);
        Log.e(TAG, "Title ==>>" + bundle.getString("TvShowName"));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        initUI();
        makeNewTvShowDetailsRequest();
        makeNewBackDropRequest(tvShowID);
        makeNewRequestThumbnail();
        makeNewSimilarTvShowsRequest(0);
        makeTvShowCastRequest();

    }

    @Override
    public void OnObjectClick(Object object) {
        Intent i = new Intent(ActivityTvShowDetails.this, ActivityTvShowDetails.class);
        Bundle b = new Bundle();
        b.putString("TvShowID", ((TvShow) object).getId());
        b.putString("TvShowName", ((TvShow) object).getName());
        i.putExtra("Bundle", b);
        startActivity(i);
    }


    @Override
    public void OnVideoClick(Video video) {
        Intent i = new Intent(this, ActivityYouTubeVideo.class);
        Bundle b = new Bundle();
        b.putString("key", video.getKey());

        i.putExtra("bundle", b);
        startActivity(i);
    }

    @Override
    public void makeRequest(int page) {
        makeNewSimilarTvShowsRequest(page);
    }

    private void initUI() {
        videosRecyclerView = findViewById(R.id.youtube_thumbnail_recycler);
        similarTvShowsRecycler = findViewById(R.id.similar_tv_show_recycler);
        tvShowCastRecycler = findViewById(R.id.recycler_tv_show_cast);
        overview = findViewById(R.id.overview);
        star = findViewById(R.id.is_watch_list);
        videoCard = findViewById(R.id.tv_show_video_card);

    }

    private void initTvShowScoreCard(TvShow tvShow) {
        scoreLayout = findViewById(R.id.score_layout);

        scoreLayout.addView(generateScoreView(R.drawable.ic_the_movie_db, tvShow.getVoteAverage()));

        ServiceOperations.serviceReq(AppController.getContext(),
                Request.Method.GET, RequestGenerator.GenerateIMDbRequestViaName(tvShow.getName().replaceAll(" ", "+")), null, new ServiceCallback() {
                    @Override
                    public void done(String result, ServiceException e) {
                        imdb = AppController.getGson().fromJson(result, IMDb.class);

                        scoreLayout.addView(generateScoreView(R.drawable.ic_imdb, imdb.getImdbRating()));
                        scoreLayout.addView(generateScoreView(R.drawable.ic_metacritic, imdb.getMetascore()));
                        if (imdb.getRunTime() != null) {
                            identityLayout.addView(generateIdentityView("Episode Run Time", imdb.getRunTime()));
                        }

                    }
                });

    }

    public String setScoreLength(String score) {
        String newScore;
        if (score.length() >= 3) {
            newScore = score.substring(0, 3);
            return newScore;
        } else {
            return score;
        }

    }

    public View generateScoreView(int image, String score) {
        View scoreView = inflater.inflate(R.layout.placement_score, scoreLayout, false);
        TextView scoreText = scoreView.findViewById(R.id.score_text);
        ImageView scoreIcon = scoreView.findViewById(R.id.score_company_icon);
        if (score != null) {
            scoreText.setText(setScoreLength(score));
            if (score.equals("N/A")) {
                scoreText.setText("- -");
                scoreText.setTextSize(10);
            }
        } else {
            scoreText.setText("- -");
        }

        scoreIcon.setImageResource(image);

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(AppController.ConvertDpToPixel(10), 0, AppController.ConvertDpToPixel(35), 0);

        if (image != R.drawable.ic_metacritic) {
            scoreView.setLayoutParams(params);
        }

        return scoreView;
    }

    public View generateIdentityView(String feature, String data) {
        View view = inflater.inflate(R.layout.placement_identity, identityLayout, false);
        TextView featureText = view.findViewById(R.id.feature);
        TextView dataText = view.findViewById(R.id.data);

        featureText.setTextColor(getResources().getColor(R.color.textColor));
        dataText.setTextColor(getResources().getColor(R.color.textColor));

        featureText.setText(feature);
        dataText.setText(data);

        return view;
    }

    private void makeNewTvShowDetailsRequest() {
        String offset = RequestGenerator.GenerateTvShowDetailsRequest(tvShowID);
        Log.e(TAG, "movie detail offset ==>>   " + offset);
        ServiceOperations.serviceReq(AppController.getContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                tvShow = AppController.getGson().fromJson(result, TvShow.class);
                Log.e(TAG, "movie title" + tvShow.getName());
                overview.setText(tvShow.getOverview());

                String _genre = "";
                collapsingToolbarLayout.setTitle(tvShow.getOriginalName());
                initTvShowScoreCard(tvShow);

                for (int i = 0; i < tvShow.getGenres().size(); i++) {
                    _genre = _genre + tvShow.getGenres().get(i).getName();
                    if (i != tvShow.getGenres().size() - 1) {
                        _genre = _genre + ", ";
                    }
                }


                if (ProfileHelper.searchOnFollowing(tvShow)) {
                    star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.orange700));
                } else {
                    star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.blueGrey200));
                }

                star.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ProfileHelper.searchOnFollowing(tvShow)) {
                            tvShow.setFavorite(true);
                            tvShow.setFavorite(!(tvShow.isFavorite()));
                            ProfileHelper.removeFromFollowing(tvShow);
                            AppController.getEventBus().post(new AddRemoveFollowingEvent(tvShow, false));
                            star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.blueGrey200));

                        } else if (!ProfileHelper.searchOnFollowing(tvShow)) {
                            tvShow.setFavorite(false);
                            tvShow.setFavorite(!(tvShow.isFavorite()));
                            ProfileHelper.addToFollowing(tvShow);
                            AppController.getEventBus().post(new AddRemoveFollowingEvent(tvShow, false));
                            star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.orange700));

                        }
                    }
                });


                identityLayout = findViewById(R.id.identity_layout);
                identityLayout.addView(generateIdentityView("Genres", _genre));
                identityLayout.addView(generateIdentityView("First Air Date", AppController.dateGenerator(tvShow.getFirstAirDate())));
                identityLayout.addView(generateIdentityView("Last Air Date", AppController.dateGenerator(tvShow.getLastAirDate())));
                identityLayout.addView(generateIdentityView("Status", tvShow.getStatus()));
                identityLayout.addView(generateIdentityView("Number of Seasons", tvShow.getNumberOfSeasons()));
                identityLayout.addView(generateIdentityView("Number of Episodes", tvShow.getNumberOfEpisodes()));
                for (int i = 0; i < tvShow.getCreatedBy().size(); i++) {
                    identityLayout.addView(generateIdentityView("Created By", tvShow.getCreatedBy().get(i).getName()));
                }


            }
        });

    }


    private void makeNewSimilarTvShowsRequest(int page) {
        String offset = RequestGenerator.GenerateSimilarTvShowsRequest(tvShowID, ++page);
        Log.e(TAG, "similar Tv Shows ==>>   " + offset);

        ServiceOperations.serviceReq(AppController.getContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                RootTvShowJsonObject rootJsonObject = AppController.getGson().fromJson(result, RootTvShowJsonObject.class);

                if (similarTvShowsAdapter != null)
                    similarTvShowsAdapter.hideLoadingItem();

                if (similarTvShowsAdapter == null) {
                    LinearLayoutManager manager = new LinearLayoutManager(AppController.getContext());
                    manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    similarTvShowsRecycler.setLayoutManager(manager);
                    similarTvShowsRecycler.setItemAnimator(new DefaultItemAnimator());
                    similarTvShowsAdapter = new SimilarMoviesAdapter(AppController.getContext(),
                            similarTvShowsList, AppController.FRAGMENT_TV_SHOWS_ID, 0, ActivityTvShowDetails.this, ActivityTvShowDetails.this);
                    similarTvShowsAdapter.setPaginationEnabled(true);
                    similarTvShowsAdapter.setPaginationListener(similarTvShowsRecycler);
                    similarTvShowsRecycler.setAdapter(similarTvShowsAdapter);


                    ViewCompat.setNestedScrollingEnabled(similarTvShowsRecycler, true);

                }

                for (int i = 0; i < rootJsonObject.getResults().size(); i++) {
                    if (rootJsonObject.getResults().get(i).getPosterPath() != null) {
                        TvShow t = rootJsonObject.getResults().get(i);
                        if (ProfileHelper.searchOnFollowing(rootJsonObject.getResults().get(i))) {
                            t.setFavorite(true);
                        }
                        similarTvShowsAdapter.addItem(rootJsonObject.getResults().get(i), similarTvShowsAdapter.getItemCount());
                    }
                }

                if (rootJsonObject.getResults().size() >= 20) {
                    similarTvShowsAdapter.setPaginationEnabled(true);
                } else {
                    similarTvShowsAdapter.setPaginationEnabled(false);
                }
            }
        });

    }

    private void makeNewRequestThumbnail() {
        String offset = RequestGenerator.GenerateTvShowVideos(tvShowID);

        ServiceOperations.serviceReq(AppController.getContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                RootVideoObject rootVideoObject = AppController.getGson().fromJson(result,
                        RootVideoObject.class);
                if (rootVideoObject.getResults().size() != 0) {
                    LinearLayoutManager mManager = new LinearLayoutManager(getApplicationContext());
                    mManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    videosRecyclerView.setLayoutManager(mManager);
                    videosRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    thumbnailAdapter = new TrailersRecyclerViewAdapter(getApplicationContext(),
                            videos, ActivityTvShowDetails.this, ActivityTvShowDetails.this);
                    videosRecyclerView.setAdapter(thumbnailAdapter);
                    ViewCompat.setNestedScrollingEnabled(videosRecyclerView, true);

                    for (int i = 0; i < rootVideoObject.getResults().size(); i++) {
                        if (rootVideoObject.getResults().get(i).getKey() != null) {
                            thumbnailAdapter.addItem(rootVideoObject.getResults().get(i), thumbnailAdapter.getItemCount());
                        }
                    }
                } else {
                    videoCard.removeView(videosRecyclerView);
                    TextView t = new TextView(ActivityTvShowDetails.this);
                    t.setTextColor(getResources().getColor(R.color.textColor));
                    t.setText(getResources().getText(R.string.text_no_trailer));
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                            FrameLayout.LayoutParams.MATCH_PARENT);
                    params.setMargins(10, AppController.ConvertDpToPixel(30), 10, AppController.ConvertDpToPixel(20));
                    params.gravity = Gravity.CENTER;
                    videoCard.addView(t, params);
                }


            }
        });
    }

    private void makeNewBackDropRequest(String tvShowID) {
        Log.e(TAG, " ==>>");
        String offset = RequestGenerator.GenerateTvShowBackDrops(tvShowID);
        ServiceOperations.serviceReq(AppController.getContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                Log.e(TAG, " result  ==>>  " + result);
                Gson gson = AppController.getGson();
                RootImageJsonObject rootImageJsonObject = gson.fromJson(result, RootImageJsonObject.class);

                ArrayList<Image> backdrops = new ArrayList<>();


                if (rootImageJsonObject.getBackDrops().size() >= 10) {
                    for (int i = 0; i < 10; i++) {
                        if (rootImageJsonObject.getBackDrops().get(i) == null) {
                            break;
                        }
                        backdrops.add(rootImageJsonObject.getBackDrops().get(i));
                    }
                } else {
                    backdrops = rootImageJsonObject.getBackDrops();
                }

                if (pagerAdapter == null) {
                    pagerAdapter = new ImageSliderAdapter(backdrops);
                }

                if (mViewPager == null) {
                    mViewPager = findViewById(R.id.tv_show_details_view_pager);

                    CollapsingToolbarLayout.LayoutParams mPagerParams = (CollapsingToolbarLayout.LayoutParams) mViewPager.getLayoutParams();
                    mPagerParams.height = AppController.DROPS_PAGER_HEIGHT;
                    mViewPager.setLayoutParams(mPagerParams);
                    initTimer();
                    mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            currentPage = position;
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });


                }

                mViewPager.setAdapter(pagerAdapter);

            }
        });
    }

    public void makeTvShowCastRequest() {
        String offset = RequestGenerator.GenerateTvShowCreditsRequest(tvShowID);
        ServiceOperations.serviceReq(getApplicationContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                RootCreditsObject creditsObject = AppController.getGson().fromJson(result, RootCreditsObject.class);
                if (tvShowCastAdapter != null)
                    tvShowCastAdapter.hideLoadingItem();

                if (tvShowCastAdapter == null) {
                    LinearLayoutManager manager = new LinearLayoutManager(AppController.getContext());
                    manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    tvShowCastRecycler.setLayoutManager(manager);
                    tvShowCastRecycler.setItemAnimator(new DefaultItemAnimator());
                    tvShowCastAdapter = new ObjectCastAdapter(cast, AppController.getContext(), ActivityTvShowDetails.this);
                    tvShowCastAdapter.setPaginationEnabled(true);
                    tvShowCastAdapter.setPaginationListener(tvShowCastRecycler);
                    tvShowCastRecycler.setAdapter(tvShowCastAdapter);


                    ViewCompat.setNestedScrollingEnabled(tvShowCastRecycler, true);

                }

                for (int i = 0; i < creditsObject.getCast().size(); i++) {
                    if (creditsObject.getCast().get(i).getProfilePath() != null) {
                        tvShowCastAdapter.addItem(creditsObject.getCast().get(i), tvShowCastAdapter.getItemCount());
                    }
                }

                tvShowCastAdapter.setPaginationEnabled(false);

            }
        });
    }


    public void initTimer() {
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == pagerAdapter.getBackDrops().size()) {
                    currentPage = 0;
                }
                mViewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
    }
}
