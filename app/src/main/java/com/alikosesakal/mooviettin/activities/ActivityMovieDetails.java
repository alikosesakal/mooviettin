package com.alikosesakal.mooviettin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.adapters.ImageSliderAdapter;
import com.alikosesakal.mooviettin.adapters.ObjectCastAdapter;
import com.alikosesakal.mooviettin.adapters.SimilarMoviesAdapter;
import com.alikosesakal.mooviettin.adapters.TrailersRecyclerViewAdapter;
import com.alikosesakal.mooviettin.customViews.CustomObjectCard;
import com.alikosesakal.mooviettin.customViews.CustomVideoCard;
import com.alikosesakal.mooviettin.events.AddRemoveWatchListEvent;
import com.alikosesakal.mooviettin.models.IMDb;
import com.alikosesakal.mooviettin.models.Image;
import com.alikosesakal.mooviettin.models.Movie;
import com.alikosesakal.mooviettin.models.RootCreditsObject;
import com.alikosesakal.mooviettin.models.RootImageJsonObject;
import com.alikosesakal.mooviettin.models.RootMovieJsonObject;
import com.alikosesakal.mooviettin.models.RootVideoObject;
import com.alikosesakal.mooviettin.models.Video;
import com.alikosesakal.mooviettin.util.AppController;
import com.alikosesakal.mooviettin.util.ProfileHelper;
import com.alikosesakal.mooviettin.util.RequestGenerator;
import com.alikosesakal.mooviettin.util.response.AdapterBase;
import com.alikosesakal.mooviettin.util.response.ServiceCallback;
import com.alikosesakal.mooviettin.util.response.ServiceException;
import com.alikosesakal.mooviettin.util.response.ServiceOperations;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ActivityMovieDetails extends AppCompatActivity implements AdapterBase.OnLoadListener
        , CustomVideoCard.OnItemVideoClickListener, CustomObjectCard.OnItemObjectClickListener {
    private String TAG = this.getClass().getSimpleName();
    /*************************
     * ADAPTERS
     ************************/
    private ImageSliderAdapter pagerAdapter;
    private TrailersRecyclerViewAdapter thumbnailAdapter;
    private SimilarMoviesAdapter similarMoviesAdapter;
    private ObjectCastAdapter castAdapter;

    /*************************
     * UI
     ************************/
    private CardView videoCard;
    private TextView overview, tagLine;
    private ViewPager mViewPager;
    private RecyclerView videosRecyclerView, similarMoviesRecycler, castRecyclerView;
    private LinearLayout scoreLayout, identityLayout;
    private ImageButton star;
    private CollapsingToolbarLayout mCollapsingToolbar;
    /*************************
     * OBJECTS
     ************************/
    private ArrayList<Object> similarMoviesList = new ArrayList<>();
    private ArrayList<Object> videos = new ArrayList<>();
    private ArrayList<Object> cast = new ArrayList<>();
    private int currentPage;
    private String movieID;
    private Movie movie;
    private IMDb imdb;
    private LayoutInflater inflater = (LayoutInflater) AppController.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("Bundle");
        movieID = bundle.getString("MovieID");

        Toolbar mToolBar = findViewById(R.id.movie_details_toolbar);
        mCollapsingToolbar = findViewById(R.id.collapsing_toolbar);
        setSupportActionBar(mToolBar);
        Log.e(TAG, "Title ==>>" + bundle.getString("MovieName"));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        initUI();
        makeNewMovieDetailsRequest();
        makeNewBackDropRequest(movieID);
        makeNewRequestThumbnail();
        makeMovieCastRequest();
        makeNewSimilarMoviewRequest(0);


        Log.e(TAG, " ==>>");
    }

    private void initUI() {
        videosRecyclerView = findViewById(R.id.youtube_thumbnail_recycler);
        similarMoviesRecycler = findViewById(R.id.similar_movies_recycler);
        castRecyclerView = findViewById(R.id.recycler_movie_cast);
        overview = findViewById(R.id.overview);
        tagLine = findViewById(R.id.tag_line);
        star = findViewById(R.id.is_watch_list);
        identityLayout = findViewById(R.id.identity_layout);
        videoCard = findViewById(R.id.movie_video_card);


    }

    private void initMovieScoreCard(Movie movie) {
        scoreLayout = findViewById(R.id.score_layout);

        scoreLayout.addView(generateScoreView(R.drawable.ic_the_movie_db, movie.getVoteAverage()));

        ServiceOperations.serviceReq(AppController.getContext(),
                Request.Method.GET, RequestGenerator.GenerateIMDbRequestViaImdbID(movie.getImdbID()), null, new ServiceCallback() {
                    @Override
                    public void done(String result, ServiceException e) {
                        imdb = AppController.getGson().fromJson(result, IMDb.class);

                        scoreLayout.addView(generateScoreView(R.drawable.ic_imdb, imdb.getImdbRating()));
                        Log.e(TAG, "metascore == " + imdb.getMetascore());
                        scoreLayout.addView(generateScoreView(R.drawable.ic_metacritic, imdb.getMetascore()));
                        identityLayout.addView(generateIdentityView("Director", imdb.getDirector()));

                    }
                });

    }

    public String setScoreLength(String score) {
        String newScore;
        if (score.length() >= 3) {
            newScore = score.substring(0, 3);
            return newScore;
        } else {
            return score;
        }

    }

    /*********************************
     * GENERATING SCORE TYPES
     ***********************************/

    public View generateScoreView(int image, String score) {

        View scoreView = inflater.inflate(R.layout.placement_score, scoreLayout, false);
        TextView scoreText = scoreView.findViewById(R.id.score_text);
        ImageView scoreIcon = scoreView.findViewById(R.id.score_company_icon);

        TextViewCompat.setAutoSizeTextTypeWithDefaults(scoreText, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);

        if (score != null) {
            scoreText.setText(setScoreLength(score));
            if (score.equals("N/A")) {
                scoreText.setText("- -");
                scoreText.setTextSize(10);
            }
        } else {
            scoreText.setText("- -");
        }
        scoreIcon.setImageResource(image);

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(AppController.ConvertDpToPixel(10), 0, AppController.ConvertDpToPixel(20), 0);

        if (image != R.drawable.ic_metacritic) {
            scoreView.setLayoutParams(params);
        }

        return scoreView;
    }

    public View generateIdentityView(String feature, String data) {
        View view = inflater.inflate(R.layout.placement_identity, identityLayout, false);
        TextView featureText = view.findViewById(R.id.feature);
        TextView dataText = view.findViewById(R.id.data);
        featureText.setTextColor(getResources().getColor(R.color.textColor));
        dataText.setTextColor(getResources().getColor(R.color.textColor));
        featureText.setText(feature);
        dataText.setText(data);

        return view;
    }

    @Override
    public void makeRequest(int page) {
        makeNewSimilarMoviewRequest(page);
    }

    private void makeNewMovieDetailsRequest() {
        String offset = RequestGenerator.GenerateMovieDetails(movieID);
        Log.e(TAG, "movie detail offset ==>>   " + offset);
        ServiceOperations.serviceReq(AppController.getContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                movie = AppController.getGson().fromJson(result, Movie.class);
                Log.e(TAG, "movie title" + movie.getTitle());
                overview.setText(movie.getOverView());
                if (movie.getTagLine() != null && !movie.getTagLine().equals("")) {
                    tagLine.setText((movie.getTagLine()));
                }

                String _genre = "";
                mCollapsingToolbar.setTitle(movie.getOriginalTitle());
                initMovieScoreCard(movie);

                for (int i = 0; i < movie.getGenres().size(); i++) {
                    _genre = _genre + movie.getGenres().get(i).getName();
                    if (i != movie.getGenres().size() - 1) {
                        _genre = _genre + ", ";
                    }
                }

                if (ProfileHelper.searchOnWathcList(movie)) {
                    star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.orange700));
                } else {
                    star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.blueGrey200));
                }

                star.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ProfileHelper.searchOnWathcList(movie)) {
                            movie.setFavorited(true);
                            movie.setFavorited(!(movie.isFavorited()));
                            ProfileHelper.removeFromWatchList(movie);
                            AppController.getEventBus().post(new AddRemoveWatchListEvent(movie, false));
                            star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.blueGrey200));
                        } else if (!ProfileHelper.searchOnWathcList(movie)) {
                            movie.setFavorited(false);
                            movie.setFavorited(!(movie.isFavorited()));
                            ProfileHelper.addToWatchList(movie);
                            AppController.getEventBus().post(new AddRemoveWatchListEvent(movie, false));
                            star.setColorFilter(ContextCompat.getColor(AppController.getContext(), R.color.orange700));
                        }
                    }
                });

                identityLayout.addView(generateIdentityView("Genres", _genre));
                int hour = 0, min = 0;
                if (movie.getRuntime() != null) {
                    int runtime = Integer.parseInt(movie.getRuntime());
                    hour = runtime / 60;
                    min = runtime - (hour * 60);
                }

                identityLayout.addView(generateIdentityView("Run Time", hour + " h  " + min + " m"));
                identityLayout.addView(generateIdentityView("Release Date", AppController.dateGenerator(movie.getReleaseDate())));

            }
        });

    }

    private void makeNewSimilarMoviewRequest(int page) {
        String offset = RequestGenerator.GenerateSimilarMovies(movieID, ++page);
        Log.e(TAG, "similar Movies ==>>   " + offset);

        ServiceOperations.serviceReq(AppController.getContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                RootMovieJsonObject baseMovieResponse = AppController.getGson().fromJson(result, RootMovieJsonObject.class);

                if (similarMoviesAdapter != null)
                    similarMoviesAdapter.hideLoadingItem();

                if (similarMoviesAdapter == null) {
                    LinearLayoutManager manager = new LinearLayoutManager(AppController.getContext());
                    manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    similarMoviesRecycler.setLayoutManager(manager);
                    similarMoviesRecycler.setItemAnimator(new DefaultItemAnimator());
                    similarMoviesAdapter = new SimilarMoviesAdapter(AppController.getContext(),
                            similarMoviesList, AppController.FRAGMENT_MOVIES_ID, 0, ActivityMovieDetails.this, ActivityMovieDetails.this);
                    similarMoviesAdapter.setPaginationEnabled(true);
                    similarMoviesAdapter.setPaginationListener(similarMoviesRecycler);
                    similarMoviesRecycler.setAdapter(similarMoviesAdapter);


                    ViewCompat.setNestedScrollingEnabled(similarMoviesRecycler, true);

                }

                for (int i = 0; i < baseMovieResponse.getResults().size(); i++) {
                    if (baseMovieResponse.getResults().get(i).getPosterPath() != null) {
                        Movie m = baseMovieResponse.getResults().get(i);
                        if (ProfileHelper.searchOnWathcList(baseMovieResponse.getResults().get(i))) {
                            m.setFavorited(true);
                        }
                        similarMoviesAdapter.addItem(baseMovieResponse.getResults().get(i), similarMoviesAdapter.getItemCount());
                    }
                }

                if (baseMovieResponse.getResults().size() >= 20) {
                    similarMoviesAdapter.setPaginationEnabled(true);
                } else {
                    similarMoviesAdapter.setPaginationEnabled(false);
                }
            }
        });

    }

    private void makeNewRequestThumbnail() {
        String offset = RequestGenerator.GenerateMovieVideos(movieID);

        ServiceOperations.serviceReq(AppController.getContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                RootVideoObject rootVideoObject = AppController.getGson().fromJson(result, RootVideoObject.class);
                if (rootVideoObject.getResults().size() != 0) {
                    if (thumbnailAdapter == null) {
                        LinearLayoutManager mManager = new LinearLayoutManager(getApplicationContext());
                        mManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                        videosRecyclerView.setLayoutManager(mManager);
                        videosRecyclerView.setItemAnimator(new DefaultItemAnimator());
                        thumbnailAdapter = new TrailersRecyclerViewAdapter(getApplicationContext(), videos, ActivityMovieDetails.this, ActivityMovieDetails.this);
                        videosRecyclerView.setAdapter(thumbnailAdapter);
                        ViewCompat.setNestedScrollingEnabled(videosRecyclerView, true);
                    }

                    for (int i = 0; i < rootVideoObject.getResults().size(); i++) {
                        if (rootVideoObject.getResults().get(i).getKey() != null) {
                            thumbnailAdapter.addItem(rootVideoObject.getResults().get(i), thumbnailAdapter.getItemCount());
                        }
                    }
                } else {
                    videoCard.removeView(videosRecyclerView);
                    TextView t = new TextView(ActivityMovieDetails.this);
                    t.setTextColor(getResources().getColor(R.color.textColor));
                    t.setText(getResources().getText(R.string.text_no_trailer));
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                            FrameLayout.LayoutParams.MATCH_PARENT);
                    params.setMargins(10, AppController.ConvertDpToPixel(30), 10, AppController.ConvertDpToPixel(20));
                    params.gravity = Gravity.CENTER;
                    videoCard.addView(t, params);

                }

            }
        });
    }

    private void makeNewBackDropRequest(String movieID) {
        Log.e(TAG, " ==>>");
        String offset = RequestGenerator.GenerateBackDropsPath(movieID);
        ServiceOperations.serviceReq(AppController.getContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                Log.e(TAG, " result  ==>>  " + result);
                Gson gson = AppController.getGson();
                RootImageJsonObject rootImageJsonObject = gson.fromJson(result, RootImageJsonObject.class);

                ArrayList<Image> backdrops = new ArrayList<>();

                if (rootImageJsonObject.getBackDrops().size() >= 10) {
                    for (int i = 0; i < 10; i++) {
                        if (rootImageJsonObject.getBackDrops().get(i) == null) {
                            break;
                        }
                        backdrops.add(rootImageJsonObject.getBackDrops().get(i));
                    }
                } else {
                    backdrops = rootImageJsonObject.getBackDrops();
                }

                if (pagerAdapter == null) {
                    pagerAdapter = new ImageSliderAdapter(backdrops);
                }

                if (mViewPager == null) {
                    mViewPager = findViewById(R.id.movie_details_view_pager);


                    CollapsingToolbarLayout.LayoutParams mPagerParams = (CollapsingToolbarLayout.LayoutParams) mViewPager.getLayoutParams();
                    mPagerParams.height = AppController.DROPS_PAGER_HEIGHT;
                    mViewPager.setLayoutParams(mPagerParams);

                    initTimer();
                    mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            currentPage = position;
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });


                }

                mViewPager.setAdapter(pagerAdapter);

            }
        });
    }

    public void makeMovieCastRequest() {
        String offset = RequestGenerator.GenerateMovieCreditsRequest(movieID);
        ServiceOperations.serviceReq(getApplicationContext(), Request.Method.GET, offset, null, new ServiceCallback() {
            @Override
            public void done(String result, ServiceException e) {
                RootCreditsObject creditsObject = AppController.getGson().fromJson(result, RootCreditsObject.class);
                if (castAdapter != null)
                    castAdapter.hideLoadingItem();

                if (castAdapter == null) {
                    LinearLayoutManager manager = new LinearLayoutManager(AppController.getContext());
                    manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    castRecyclerView.setLayoutManager(manager);
                    castRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    castAdapter = new ObjectCastAdapter(cast, AppController.getContext(), ActivityMovieDetails.this);
                    castAdapter.setPaginationEnabled(true);
                    castAdapter.setPaginationListener(castRecyclerView);
                    castRecyclerView.setAdapter(castAdapter);


                    ViewCompat.setNestedScrollingEnabled(similarMoviesRecycler, true);

                }

                for (int i = 0; i < creditsObject.getCast().size(); i++) {
                    if (creditsObject.getCast().get(i).getProfilePath() != null) {
                        castAdapter.addItem(creditsObject.getCast().get(i), castAdapter.getItemCount());
                    }
                }

                castAdapter.setPaginationEnabled(false);

            }
        });
    }

    public void initTimer() {
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == pagerAdapter.getBackDrops().size()) {
                    currentPage = 0;
                }
                mViewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
    }

    @Override
    public void OnVideoClick(Video video) {
        Intent i = new Intent(this, ActivityYouTubeVideo.class);
        Bundle b = new Bundle();
        b.putString("key", video.getKey());
        i.putExtra("bundle", b);
        startActivity(i);
    }

    @Override
    public void OnObjectClick(Object object) {
        Intent i = new Intent(ActivityMovieDetails.this, ActivityMovieDetails.class);
        Bundle b = new Bundle();
        b.putString("MovieID", ((Movie) object).getId());
        i.putExtra("Bundle", b);
        startActivity(i);
    }

}
