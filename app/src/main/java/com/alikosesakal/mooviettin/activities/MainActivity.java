package com.alikosesakal.mooviettin.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.alikosesakal.mooviettin.BaseActivity;
import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.adapters.ViewPagerAdapter;
import com.alikosesakal.mooviettin.fragments.FragmentMovie;
import com.alikosesakal.mooviettin.fragments.FragmentProfile;
import com.alikosesakal.mooviettin.fragments.FragmentTvShows;

public class MainActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private int[] icons = {R.drawable.ic_movie, R.drawable.ic_tv, R.drawable.ic_profile};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = findViewById(R.id.toolbar);
        mViewPager = findViewById(R.id.view_pager);
        mTabLayout = findViewById(R.id.tabs);

        setSupportActionBar(mToolbar);

        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);


        mTabLayout.getTabAt(0).setIcon(icons[0]);
        mTabLayout.getTabAt(1).setIcon(icons[1]);
        mTabLayout.getTabAt(2).setIcon(icons[2]);


    }

    public void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mAdapter.addFragment(new FragmentMovie(), "Movie");
        mAdapter.addFragment(new FragmentTvShows(), "Tv Shows");
        mAdapter.addFragment(new FragmentProfile(), "profile");
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(mAdapter);
    }
}
