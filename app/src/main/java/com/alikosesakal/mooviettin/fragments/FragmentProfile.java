package com.alikosesakal.mooviettin.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.adapters.ViewPagerAdapter;
import com.alikosesakal.mooviettin.util.AppController;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProfile extends Fragment {

    private ViewPager mPager;
    private TabLayout mTabLayout;
    private ViewPagerAdapter mAdapter;


    public FragmentProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        container = (ViewGroup) inflater.inflate(R.layout.fragment_profile, container, false);

        initUI(container);
        return container;
    }

    public void initUI(ViewGroup container) {
        mPager = container.findViewById(R.id.profile_view_pager);
        mTabLayout = container.findViewById(R.id.movie_tab_layout);
        mAdapter = new ViewPagerAdapter(getFragmentManager());
        mAdapter.addFragment(FragmentChild.newInstance(AppController.FRAGMENT_PROFILE_ID,
                AppController.FRAGMENT_WATCH_LIST), "WATCH LIST");
        mAdapter.addFragment(FragmentChild.newInstance(AppController.FRAGMENT_PROFILE_ID,
                AppController.FRAGMENT_FOLLOWING), "FOLLOWING");

        mPager.setOffscreenPageLimit(2);
        mPager.setAdapter(mAdapter);

        mTabLayout.setupWithViewPager(mPager);
    }

}
