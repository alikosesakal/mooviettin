package com.alikosesakal.mooviettin.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.adapters.ViewPagerAdapter;
import com.alikosesakal.mooviettin.util.AppController;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTvShows extends Fragment {
    private ViewPager mPager;
    private TabLayout mTabLayout;
    private ViewPagerAdapter mAdapter;


    public FragmentTvShows() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        container = (ViewGroup) inflater.inflate(R.layout.fragment_tv_shows, container, false);

        initUI(container);

        return container;
    }

    public void initUI(ViewGroup container) {
        mPager = container.findViewById(R.id.tv_show_view_pager);
        mTabLayout = container.findViewById(R.id.tv_show_tab_layout);
        mAdapter = new ViewPagerAdapter(getFragmentManager());
        mAdapter.addFragment(FragmentChild.newInstance(AppController.FRAGMENT_TV_SHOWS_ID,
                AppController.FRAGMENT_TODAY), "Today");
        mAdapter.addFragment(FragmentChild.newInstance(AppController.FRAGMENT_TV_SHOWS_ID,
                AppController.FRAGMENT_ON_THE_AIR), "On The Air");
        mAdapter.addFragment(FragmentChild.newInstance(AppController.FRAGMENT_TV_SHOWS_ID,
                AppController.FRAGMENT_TV_POPULAR), "Tv Popular");
        mAdapter.addFragment(FragmentChild.newInstance(AppController.FRAGMENT_TV_SHOWS_ID,
                AppController.FRAGMENT_TV_TOP_RATED), "Tv Top Rated");

        mPager.setOffscreenPageLimit(4);
        mPager.setAdapter(mAdapter);

        mTabLayout.setupWithViewPager(mPager);
    }

}
