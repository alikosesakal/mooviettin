package com.alikosesakal.mooviettin.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.alikosesakal.mooviettin.BaseFragment;
import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.activities.ActivityMovieDetails;
import com.alikosesakal.mooviettin.activities.ActivityTvShowDetails;
import com.alikosesakal.mooviettin.adapters.ObjectsRecyclerViewAdapter;
import com.alikosesakal.mooviettin.customViews.CustomObjectCard;
import com.alikosesakal.mooviettin.events.AddRemoveFollowingEvent;
import com.alikosesakal.mooviettin.events.AddRemoveWatchListEvent;
import com.alikosesakal.mooviettin.models.Movie;
import com.alikosesakal.mooviettin.models.RootMovieJsonObject;
import com.alikosesakal.mooviettin.models.RootTvShowJsonObject;
import com.alikosesakal.mooviettin.models.TvShow;
import com.alikosesakal.mooviettin.util.AppController;
import com.alikosesakal.mooviettin.util.CustomGridLayoutManager;
import com.alikosesakal.mooviettin.util.GridSpacingItemDecoration;
import com.alikosesakal.mooviettin.util.ProfileHelper;
import com.alikosesakal.mooviettin.util.RequestGenerator;
import com.alikosesakal.mooviettin.util.response.AdapterBase;
import com.alikosesakal.mooviettin.util.response.ServiceCallback;
import com.alikosesakal.mooviettin.util.response.ServiceException;
import com.alikosesakal.mooviettin.util.response.ServiceOperations;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentChild extends BaseFragment implements AdapterBase.OnLoadListener,
        CustomObjectCard.OnItemObjectClickListener {

    private final String TAG = this.getClass().getSimpleName();
    private BroadcastReceiver mReciever;
    private RecyclerView mRecyclerView;
    private ObjectsRecyclerViewAdapter mAdapter;
    /**************
     * Objects are movie or tv show
     ****************/
    private ArrayList<Object> objects = new ArrayList<>();
    private int fragmentID, typeID;


    public FragmentChild() {
        // Required empty public constructor
    }

    public static FragmentChild newInstance(int typeID, int fragmentID) {
        FragmentChild f = new FragmentChild();
        Bundle b = new Bundle();
        b.putInt("fragmentID", fragmentID);
        b.putInt("typeID", typeID);
        f.setArguments(b);
        return f;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        container = (ViewGroup) inflater.inflate(R.layout.fragment_child, container, false);
        if (getArguments() != null) {
            typeID = getArguments().getInt("typeID", 1);
            fragmentID = getArguments().getInt("fragmentID", 0);
        } else {
            Log.e(TAG, "getArguments is null..");
        }

        mRecyclerView = container.findViewById(R.id.recycler_container);

        IntentFilter mFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);

        mReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
                if (!noConnectivity) {
                    switch (typeID) {
                        case AppController.FRAGMENT_MOVIES_ID:
                            makeNewMovieRequest(0);
                            Log.e(TAG, "typeID ==>> " + typeID);
                            Log.e(TAG, "fragmentID: " + fragmentID);
                            break;
                        case AppController.FRAGMENT_TV_SHOWS_ID:
                            makeNewTvShowRequest(0);
                            Log.e(TAG, "typeID ==>> " + typeID);
                            Log.e(TAG, "fragmentID: " + fragmentID);
                            break;
                        case AppController.FRAGMENT_PROFILE_ID:
                            if (fragmentID == AppController.FRAGMENT_WATCH_LIST) {
                                getWatchList();
                                Log.e(TAG, "typeID ==>> " + typeID);
                                Log.e(TAG, "fragmentID: " + fragmentID);
                            } else if (fragmentID == AppController.FRAGMENT_FOLLOWING) {
                                getFollowing();
                                Log.e(TAG, "typeID ==>> " + typeID);
                                Log.e(TAG, "fragmentID: " + fragmentID);
                            }
                            break;
                    }
                } else {
                    Toast.makeText(AppController.getContext()
                            , "There is no internet connection!!!", Toast.LENGTH_LONG).show();
                }

            }
        };

        AppController.getContext().registerReceiver(mReciever, mFilter);
        return container;
    }

    @Override
    public void OnObjectClick(Object object) {

        if (typeID == AppController.FRAGMENT_MOVIES_ID || fragmentID == AppController.FRAGMENT_WATCH_LIST) {
            Intent i = new Intent(getActivity(), ActivityMovieDetails.class);
            Bundle b = new Bundle();
            b.putString("MovieID", ((Movie) object).getId());
            b.putString("MovieName", ((Movie) object).getOriginalTitle());
            i.putExtra("Bundle", b);
            startActivity(i);
        } else if (typeID == AppController.FRAGMENT_TV_SHOWS_ID || fragmentID == AppController.FRAGMENT_FOLLOWING) {
            Intent i = new Intent(getActivity(), ActivityTvShowDetails.class);
            Bundle b = new Bundle();
            b.putString("TvShowID", ((TvShow) object).getId());
            b.putString("TvShowName", ((TvShow) object).getOriginalName());
            i.putExtra("Bundle", b);
            startActivity(i);
        }

    }

    /*********************************
     * LISTENERS
     ********************************/

    @Override
    public void makeRequest(int page) {
        if (typeID == AppController.FRAGMENT_MOVIES_ID) {
            makeNewMovieRequest(page);
        } else if (typeID == AppController.FRAGMENT_TV_SHOWS_ID) {
            makeNewTvShowRequest(page);
        }

    }

    /*********************************
     * REQUESTS ( MOVIE AND TV SHOWS )
     ********************************/

    public void makeNewMovieRequest(int page) {
        String offset = RequestGenerator.GenerateMoviesRequest(typeID, fragmentID, ++page);
        ServiceOperations.serviceReq(getContext(), Request.Method.GET,
                offset, null, new ServiceCallback() {
                    @Override
                    public void done(String result, ServiceException e) {
                        if (mAdapter != null) {
                            mAdapter.hideLoadingItem();
                        }

                        RootMovieJsonObject response = AppController.getGson().fromJson(result, RootMovieJsonObject.class);

                        if (mAdapter == null) {
                            CustomGridLayoutManager mManager = new CustomGridLayoutManager(getContext(), 2);
                            mRecyclerView.setLayoutManager(mManager);
                            mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, AppController.ConvertDpToPixel(10), true, getContext()));
                            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                            mAdapter = new ObjectsRecyclerViewAdapter(getContext(), objects, typeID, fragmentID, FragmentChild.this, FragmentChild.this);
                            mAdapter.setPaginationEnabled(true);
                            mAdapter.setPaginationListener(mRecyclerView);
                            mAdapter.setLoadingSpanSize(2);
                            mRecyclerView.setAdapter(mAdapter);

                            ViewCompat.setNestedScrollingEnabled(mRecyclerView, true);
                        }

                        ProfileHelper.adaptWatchList(response.getResults());

                        for (int i = 0; i < response.getResults().size(); i++) {
                            if (response.getResults().get(i).getPosterPath() != null) {
                                mAdapter.addItem(response.getResults().get(i), mAdapter.getItemCount());
                            }
                        }

                        if (response.getResults().size() >= 20) {
                            mAdapter.setPaginationEnabled(true);
                        } else {
                            mAdapter.setPaginationEnabled(false);
                        }

                    }
                });
    }

    public void makeNewTvShowRequest(int page) {
        String offset = RequestGenerator.GenerateTvShowsRequest(typeID, fragmentID, ++page);
        ServiceOperations.serviceReq(getContext(), Request.Method.GET,
                offset, null, new ServiceCallback() {
                    @Override
                    public void done(String result, ServiceException e) {
                        if (mAdapter != null) {
                            mAdapter.hideLoadingItem();
                        }

                        RootTvShowJsonObject response = AppController.getGson().fromJson(result, RootTvShowJsonObject.class);
                        if (mAdapter == null) {
                            CustomGridLayoutManager mManager = new CustomGridLayoutManager(getContext(), 2);
                            mRecyclerView.setLayoutManager(mManager);
                            mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, AppController.ConvertDpToPixel(10), true, getContext()));
                            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                            mAdapter = new ObjectsRecyclerViewAdapter(getContext(), objects, typeID, fragmentID, FragmentChild.this, FragmentChild.this);
                            mRecyclerView.setAdapter(mAdapter);

                            mAdapter.setPaginationEnabled(true);
                            mAdapter.setPaginationListener(mRecyclerView);

                            ViewCompat.setNestedScrollingEnabled(mRecyclerView, true);
                        }

                        ProfileHelper.adaptFollowing(response.getResults());

                        for (int i = 0; i < response.getResults().size(); i++) {
                            if (response.getResults().get(i).getPosterPath() != null) {
                                mAdapter.addItem(response.getResults().get(i), mAdapter.getItemCount());
                            }
                        }

                        if (response.getResults().size() >= 20) {
                            mAdapter.setPaginationEnabled(true);
                        } else {
                            mAdapter.setPaginationEnabled(false);
                        }

                    }
                });
    }

    public void getWatchList() {

        if (mAdapter == null) {
            CustomGridLayoutManager mManager = new CustomGridLayoutManager(getContext(), 2);
            mRecyclerView.setLayoutManager(mManager);
            mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, AppController.ConvertDpToPixel(10), true, getContext()));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mAdapter = new ObjectsRecyclerViewAdapter(getContext(), objects, typeID, fragmentID, FragmentChild.this, FragmentChild.this);
            mRecyclerView.setAdapter(mAdapter);

            mAdapter.setPaginationEnabled(true);
            mAdapter.setPaginationListener(mRecyclerView);

            ViewCompat.setNestedScrollingEnabled(mRecyclerView, true);
        }

        for (int i = 0; i < ProfileHelper.getWatchList().size(); i++) {
            if (ProfileHelper.getWatchList().get(i).getPosterPath() != null) {
                mAdapter.addItem(ProfileHelper.getWatchList().get(i), mAdapter.getItemCount());
            }
        }

        mAdapter.hideLoadingItem();

        if (ProfileHelper.getWatchList().size() >= 20) {
            mAdapter.setPaginationEnabled(true);

        } else {
            mAdapter.setPaginationEnabled(false);
        }

    }

    public void getFollowing() {
        if (mAdapter == null) {
            CustomGridLayoutManager mManager = new CustomGridLayoutManager(getContext(), 2);
            mRecyclerView.setLayoutManager(mManager);
            mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, AppController.ConvertDpToPixel(10), true, getContext()));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mAdapter = new ObjectsRecyclerViewAdapter(getContext(), objects, typeID, fragmentID, FragmentChild.this, FragmentChild.this);
            mRecyclerView.setAdapter(mAdapter);

            mAdapter.setPaginationEnabled(true);
            mAdapter.setPaginationListener(mRecyclerView);

            ViewCompat.setNestedScrollingEnabled(mRecyclerView, true);
        }

        for (int i = 0; i < ProfileHelper.getFollowing().size(); i++) {
            if (ProfileHelper.getFollowing().get(i).getPosterPath() != null) {
                mAdapter.addItem(ProfileHelper.getFollowing().get(i), mAdapter.getItemCount());
            }
        }

        mAdapter.hideLoadingItem();

        if (ProfileHelper.getFollowing().size() >= 20) {
            mAdapter.setPaginationEnabled(true);

        } else {
            mAdapter.setPaginationEnabled(false);
        }
    }

    @Subscribe
    public void handleWatchListEvent(AddRemoveWatchListEvent event) {

        if (typeID == AppController.FRAGMENT_PROFILE_ID && fragmentID == AppController.FRAGMENT_WATCH_LIST) {
            if (!(event.getMovie().isFavorited())) {
                int position = 0;
                for (int i = 0; i < mAdapter.getObjects().size(); i++) {
                    if (((Movie) mAdapter.getObjects().get(i)).getId().equals(event.getMovie().getId())) {
                        position = i;
                    }
                }
                mAdapter.removeItem(position);
            } else {
                mAdapter.addItem(event.getMovie(), mAdapter.getItemCount());
            }

        } else if (typeID == AppController.FRAGMENT_MOVIES_ID) {
            mAdapter.notifyItemChanged(mAdapter.notifyMovieItem(event.getMovie()));
        }

    }

    @Subscribe
    public void handleFollowingEvent(AddRemoveFollowingEvent event) {
        if (typeID == AppController.FRAGMENT_PROFILE_ID && fragmentID == AppController.FRAGMENT_FOLLOWING) {
            if (!(event.getTvShow().isFavorite())) {
                int position = 0;
                for (int i = 0; i < mAdapter.getObjects().size(); i++) {
                    if (((TvShow) mAdapter.getObjects().get(i)).getId().equals(event.getTvShow().getId())) {
                        position = i;
                    }
                }
                mAdapter.removeItem(position);
            } else {
                mAdapter.addItem(event.getTvShow(), mAdapter.getItemCount());
            }
        } else if (typeID == AppController.FRAGMENT_TV_SHOWS_ID) {
            mAdapter.notifyItemChanged(mAdapter.notifyTvShowItem(event.getTvShow()));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppController.getContext().unregisterReceiver(mReciever);
    }
}
