package com.alikosesakal.mooviettin.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.alikosesakal.mooviettin.util.response.MyVolley;
import com.squareup.otto.Bus;

/**
 * Created by ali on 07.11.2016.
 */

public class AppController extends Application {
    private static final String TAG = AppController.class.getSimpleName();
    private static final String YOUTUBE_API_KEY = "AIzaSyCZt9CJh10FBuQvIqga68QMjdB2XdDLjLw";
    private static final String OMDB_API_KEY = "49730ea8";
    public static final int FRAGMENT_MOVIES_ID = 1;
    public static final int FRAGMENT_TV_SHOWS_ID = 2;
    public static final int FRAGMENT_PROFILE_ID = 3;
    public static final int FRAGMENT_WATCH_LIST = 310;
    public static final int FRAGMENT_FOLLOWING = 320;
    public static final int FRAGMENT_UPCOMING = 110;
    public static final int FRAGMENT_NOW_PLAYING = 120;
    public static final int FRAGMENT_POPULAR = 130;
    public static final int FRAGMENT_TOP_RATED = 140;
    public static final int FRAGMENT_ON_THE_AIR = 210;
    public static final int FRAGMENT_TODAY = 220;
    public static final int FRAGMENT_TV_POPULAR = 230;
    public static final int FRAGMENT_TV_TOP_RATED = 240;
    public static final float DROPS_PAGER_ASPECT_RATIO = 0.562f;
    public static int DROPS_PAGER_HEIGHT = 0;
    public static int SCREEN_WIDTH = 0;
    public static int SCREEN_HEIGHT = 0;
    private static final String[] mounths = {"January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"};


    private RequestQueue mRequestQueue;
    private static AppController mInstance;

    private static Gson gson;
    private static Bus bus;

    private static MyVolley myVolley;
    private static SharedPreferences mSharedPrefs;
    private static SharedPreferences.Editor mPrefsEditor;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat("M/d/yy hh:mm a");
        GenerateScreenSizes(getContext());
        GenerateDropsPagerHeight();
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static int ConvertPixelToDp(int pixels) {
        DisplayMetrics displayMetrics = mInstance.getResources().getDisplayMetrics();
        Log.e(TAG, String.valueOf(displayMetrics.heightPixels));
        Log.e(TAG, String.valueOf(displayMetrics.widthPixels));
        return ((int) (pixels / displayMetrics.density));
    }

    public static int ConvertDpToPixel(int dp) {
        return Math.round(dp * mInstance.getResources().getDisplayMetrics().density);
    }

    public static Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public static Bus getEventBus() {
        if (bus == null) {
            bus = new Bus();
        }
        return bus;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    public static MyVolley getMyVolley(Context context) {
        if (myVolley == null) {
            myVolley = new MyVolley(context);
        }
        return myVolley;
    }

    public static void setMyVolley(MyVolley myVolley) {
        AppController.myVolley = myVolley;
    }

    public static Context getContext() {
        if (mInstance == null) {
            mInstance = new AppController();
        }
        return mInstance;
    }

    public static SharedPreferences getmSharedPrefs() {
        if (mSharedPrefs == null) {
            mSharedPrefs = getContext().getSharedPreferences("uygulamaVerileri",
                    MODE_PRIVATE);
        }
        return mSharedPrefs;
    }

    public static SharedPreferences.Editor getmPrefsEditor() {

        if (mPrefsEditor == null || mSharedPrefs == null) {
            mSharedPrefs = getContext().getSharedPreferences("uygulamaVerileri",
                    MODE_PRIVATE);
            mPrefsEditor = mSharedPrefs.edit();
        }
        return mPrefsEditor;
    }

    public static String getYoutubeApiKey() {
        return YOUTUBE_API_KEY;
    }

    public static String getOMDBApiKey() {
        return OMDB_API_KEY;
    }

    public static String dateGenerator(String date) {
        String newDate = "";
        if (date != null) {
            String year = date.substring(0, 4);
            String mounth = mounths[Integer.parseInt(date.substring(5, 7)) - 1];
            String day = date.substring(8, 10);
            if (day.substring(0, 1).equals("0")) {
                day = day.substring(1, 2);
            }
            newDate = day + " " + mounth + " " + year;
        } else {
            newDate = "- -";
        }

        return newDate;
    }

    public void GenerateScreenSizes(Context mContext) {
        int width = 0, height = 0;

        try {
            DisplayMetrics mMetrics = new DisplayMetrics();
            WindowManager mManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            assert mManager != null;
            mManager.getDefaultDisplay().getMetrics(mMetrics);
            width = mMetrics.widthPixels;
            height = mMetrics.heightPixels;

        } catch (Exception e) {
            Log.e(TAG + "==>> ", e.getMessage());
        }

        SCREEN_HEIGHT = height;
        SCREEN_WIDTH = width;
    }

    public void GenerateDropsPagerHeight() {
        DROPS_PAGER_HEIGHT = Math.round(SCREEN_WIDTH * DROPS_PAGER_ASPECT_RATIO);
    }

    public boolean IsOnline() {
        ConnectivityManager mManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (mManager != null) {
            NetworkInfo mInfo = mManager.getActiveNetworkInfo();
            return mInfo != null && mInfo.isConnectedOrConnecting();
        } else {
            Log.e(TAG, "mManager is null...");
            return false;
        }

    }


}
