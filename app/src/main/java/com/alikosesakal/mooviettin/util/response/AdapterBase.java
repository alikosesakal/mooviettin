package com.alikosesakal.mooviettin.util.response;

/**
 * Created by Oguzhan on 30.11.2015.
 */

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.alikosesakal.mooviettin.R;

import java.util.ArrayList;

public class AdapterBase extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String LOG_TAG = "AdapterBase";
    public static final int TYPE_LOAD = -1;
    private Context context;
    private ArrayList<Object> objects;
    private OnLoadListener onLoadListener;
    private RecyclerView recyclerView;
    private boolean isPaginationEnabled = false;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int page = 0;


    public AdapterBase(ArrayList<Object> objects, Context context, OnLoadListener onLoadListener) {
        this.context = context;
        this.onLoadListener = onLoadListener;
        this.objects = objects;
    }

    public AdapterBase(ArrayList<Object> objects, Context context) {
        this.context = context;
        this.objects = objects;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder = onCreateCustomViewHolder(parent, viewType);

        switch (viewType) {
            case TYPE_LOAD:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_progress, parent, false);
                holder = new LoadingObjectHolder(view);

        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (objects.get(position) instanceof LoadingItem) {
            Log.d("ADAPTER", "onBindViewHolder: Loading item");
            return;
        }
        onBindCustomViewHolder(holder, position);

    }

    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
    }


    public void addItem(Object dataObj, int index) {
        objects.add(dataObj);
        notifyItemInserted(index);
    }

    public void removeItem(int index) {
        objects.remove(index);
        notifyItemRemoved(index);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        }, 500);
    }

    public void removeAllItems() {
        objects.clear();
        notifyDataSetChanged();
    }

    public void hideLoadingItem() {
        for (int i = 0; i < objects.size(); i++) {
            if (objects.get(i) instanceof LoadingItem) {
                objects.remove(i);
                notifyItemRemoved(i);
                return;
            }
        }

    }

    public void addLoadingItem() {
        objects.add(new LoadingItem());

        recyclerView.post(new Runnable() {
            public void run() {
                notifyItemInserted(getItemCount() - 1);
            }
        });
    }


    public void setPaginationEnabled(boolean paginationEnabled) {
        isPaginationEnabled = paginationEnabled;
    }

    public void setPaginationListener(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        recyclerView.addOnScrollListener(paginationListener);
    }

    RecyclerView.OnScrollListener paginationListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            visibleItemCount = recyclerView.getLayoutManager().getChildCount();
            totalItemCount = recyclerView.getLayoutManager().getItemCount();

            if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            } else if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                pastVisiblesItems = ((GridLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            } else {
                int[] firstVisibleItems = null;
                firstVisibleItems = ((StaggeredGridLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPositions(firstVisibleItems);
                if (firstVisibleItems != null && firstVisibleItems.length > 0) {
                    pastVisiblesItems = firstVisibleItems[0];
                }
            }

            if (isPaginationEnabled) {
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    isPaginationEnabled = false;
                    page++;
                    addLoadingItem();
                    if (onLoadListener != null) {
                        onLoadListener.makeRequest(page);

                    }
                }
            }
        }

    };

    @Override
    public int getItemCount() {
        return objects.size();
    }


    public class LoadingObjectHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        public LoadingObjectHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.footer_progress);
            //set progress color filter
            progressBar.getIndeterminateDrawable().setColorFilter(
                    ContextCompat.getColor(context, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

    }

    public interface OnLoadListener {
        void makeRequest(int page);

    }

    public class LoadingItem {

        public LoadingItem() {
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof LoadingItem) {
            return TYPE_LOAD;
        }
        return 1;
    }

    public void setLoadingSpanSize(final int spanSize) {
        try {
            if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                ((GridLayoutManager) recyclerView.getLayoutManager()).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        switch (getItemViewType(position)) {
                            case TYPE_LOAD:
                                return spanSize;
                            default:
                                return 1;
                        }
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}