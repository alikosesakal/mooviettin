package com.alikosesakal.mooviettin.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.alikosesakal.mooviettin.models.Movie;
import com.alikosesakal.mooviettin.models.TvShow;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ali on 13.12.2016.
 */

public class SharedPrefHelper {
    private static Set<String> watchList = new HashSet<>();
    private static Set<TvShow> following = new HashSet<>();
    private static SharedPreferences sharedPrefWatchList = AppController.getContext()
            .getSharedPreferences("sharedpref", Context.MODE_PRIVATE);
    private static SharedPreferences sharedPrefFollowing = AppController.getContext()
            .getSharedPreferences("following", Context.MODE_PRIVATE);


    public static void addMovieToWatchList(Movie movie) {
        watchList.add(movie.getId());
        SharedPreferences.Editor editor = sharedPrefWatchList.edit();
        editor.clear();
        editor.putStringSet("watchlist", watchList);
        editor.apply();
    }

    public void deletMovieToWatchList(String movieID) {
        watchList.remove(movieID);

    }

    public static Set<String> getWatchList() {
        return watchList;
    }

    public static void setWatchList(Set<String> watchList) {
        SharedPrefHelper.watchList = watchList;
    }

    public static Set<TvShow> getFollowing() {
        return following;
    }

    public static void setFollowing(Set<TvShow> following) {
        SharedPrefHelper.following = following;
    }

    public static SharedPreferences getSharedPrefWatchList() {
        return sharedPrefWatchList;
    }

    public static void setSharedPrefWatchList(SharedPreferences sharedPrefWatchList) {
        SharedPrefHelper.sharedPrefWatchList = sharedPrefWatchList;
    }

    public static SharedPreferences getSharedPrefFollowing() {
        return sharedPrefFollowing;
    }

    public static void setSharedPrefFollowing(SharedPreferences sharedPrefFollowing) {
        SharedPrefHelper.sharedPrefFollowing = sharedPrefFollowing;
    }
}
