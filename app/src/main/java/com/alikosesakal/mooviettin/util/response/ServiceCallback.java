
package com.alikosesakal.mooviettin.util.response;


public interface ServiceCallback {
    public void done(String result, ServiceException e);
}
