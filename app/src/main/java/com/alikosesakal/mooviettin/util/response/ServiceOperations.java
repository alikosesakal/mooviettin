package com.alikosesakal.mooviettin.util.response;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.alikosesakal.mooviettin.util.AppController;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class ServiceOperations {
    private static String baseUrl = "";
    private static ProgressDialog pd;
    private static final String DEFAULT_PARAMS_ENCODING = "UTF-8";

    public static void serviceReq(final Context mContext, int serviceType,
                                  String offsetUrl, Map<String, String> params,
                                  final ServiceCallback callback) {
        serviceCall(mContext, serviceType, offsetUrl, params, callback, null);

    }


    private static void serviceCall(final Context mContext, int serviceType,
                                    String offsetUrl, final Map<String, String> params,
                                    final ServiceCallback callback, final String pdString) {
        if (isOnline(mContext)) {

            String url = baseUrl + offsetUrl;

//            if (serviceType == Request.Method.GET) {
//                url = url + "?"
//                        + encodeParameters(params, DEFAULT_PARAMS_ENCODING);
//            }

            Log.d("WebServis", "İstek yapılan url: " + url);
            if (pdString != null) {
                pd = ProgressDialog.show(mContext, null, pdString);
                pd.setCancelable(true);
            }

            StringRequest request = new StringRequest(serviceType, url,
                    new Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            if (pdString != null) {
                                pd.dismiss();
                            }

                            Log.d("WebServis", "Servis cevabı : " +
                                    response);
                            callback.done(response, null);
                        }
                    }, new ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (pdString != null) {
                        pd.dismiss();
                    }
                    Log.d("WebServis", "Servis cevabı : " + error);

                    String decoded = "";
                    ServiceError serviceError = null;

                    try {
                        decoded = new String(
                                error.networkResponse.data, "UTF-8");
                        Log.d("WebServis", "Servis cevabı : " +
                                decoded);
                        serviceError = AppController.getGson()
                                .fromJson(decoded, ServiceError.class);

                    } catch (Exception e) {
                        decoded = "";
                        e.printStackTrace();
                    }

                    ServiceException serviceException = null;

                    if (TextUtils.isEmpty(decoded)) {
                        serviceException = new ServiceException(
                                "Bir hata oluştu, lütfen tekrar deneyiniz.",
                                false, serviceError);
                    } else {
                        serviceException = new ServiceException(
                                serviceError.getError(), false, serviceError);
                    }

                    try {
                        if (!TextUtils.isEmpty(serviceError.code) && serviceError.code.equalsIgnoreCase("app.error.complaint.email_not_verified")) {
                            callback.done(null, serviceException);
                        } else {
                            if (error instanceof TimeoutError) {
                                Toast.makeText(mContext,
                                        "Lütfen Internet bağlantınızı kontrol ediniz.",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                if (error.networkResponse.statusCode == 503) {
                                    //Bakim yapiliyor
                                } else if (error.networkResponse.statusCode == 403) {
                                    //gecersiz token
                                } else {
                                    Toast.makeText(mContext,
                                            serviceException.getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                }
                                callback.done(null, serviceException);

                            }
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Toast.makeText(mContext,
                                "Sunucu yoğunluktan dolayı cevap veremiyor. Lütfen daha sonra tekrar deneyiniz.",
                                Toast.LENGTH_SHORT).show();
                    }


                }
            }) {

                protected Map<String, String> getParams()
                        throws AuthFailureError {
                    if (params != null) {
                        writeParams(params);
                    }

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("X-Mobillium-Token", ApplicationClass.getToken());
//                    params.put("Accept-Language", "tr");

                    return params;
                }


            };

            request.setShouldCache(false);
            AppController.getMyVolley(mContext).getRequestQueue()
                    .add(request);

        } else {

            Toast.makeText(mContext,
                    "Lütfen internet bağlantınızı kontrol ediniz.",
                    Toast.LENGTH_SHORT).show();
            callback.done(null, new ServiceException(
                    "Lütfen internet bağlantınızı kontrol ediniz.", true));
        }
    }

    public static void writeParams(Map<String, String> params) {
        Iterator<Map.Entry<String, String>> entries = params.entrySet()
                .iterator();
        while (entries.hasNext()) {
            Map.Entry<String, String> entry = entries.next();
            Log.d("WebServis", "Key = " + entry.getKey() + ", Value = " + entry.getValue());
        }
    }

    public static boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    private static String encodeParameters(Map<String, String> params,
                                           String paramsEncoding) {
        StringBuilder encodedParams = new StringBuilder();
        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                encodedParams.append(URLEncoder.encode(entry.getKey(),
                        paramsEncoding));
                encodedParams.append('=');
                encodedParams.append(URLEncoder.encode(entry.getValue(),
                        paramsEncoding));
                encodedParams.append('&');
            }
            return encodedParams.toString();
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Encoding not supported: "
                    + paramsEncoding, uee);
        }
    }


}
