package com.alikosesakal.mooviettin.util;

import com.google.gson.reflect.TypeToken;
import com.alikosesakal.mooviettin.models.Movie;
import com.alikosesakal.mooviettin.models.TvShow;

import java.lang.reflect.Type;
import java.util.ArrayList;


/**
 * Created by ali on 14.12.2016.
 */

public class ProfileHelper {
    private static ArrayList<Movie> watchList;
    private static ArrayList<TvShow> following;


    public static ArrayList<Movie> getWatchList() {
        if (watchList == null) {
            watchList = JSONToArray(AppController.getmSharedPrefs().getString("watchList", "[]"));
        }
        return watchList;
    }

    private static ArrayList<Movie> JSONToArray(String json) {
        //TODO burayı unutma
        // TODO: 16.12.2016: tamam unutmam  :D
        ArrayList<Movie> watchList;
        Type type = new TypeToken<ArrayList<Movie>>() {
        }.getType();
        watchList = AppController.getGson().fromJson(json, type);
        return watchList;
    }

    private static String arrayToJSON(ArrayList<Movie> arrayList) {
        String jsonArray;
        Type type = new TypeToken<ArrayList<Movie>>() {
        }.getType();
        jsonArray = AppController.getGson().toJson(arrayList, type);
        return jsonArray;
    }


    public static boolean addToWatchList(Movie movie) {
        if (getWatchList() != null) {
            getWatchList().add(movie);
            return AppController.getmPrefsEditor().putString("watchList", arrayToJSON(getWatchList())).commit();
        }
        return false;
    }

    public static boolean removeFromWatchList(Movie movie) {
        if (getWatchList() != null) {
            for (int i = 0; i < getWatchList().size(); i++) {
                if (getWatchList().get(i).getId().contentEquals(movie.getId())) {
                    getWatchList().remove(i);
                    return AppController.getmPrefsEditor().putString("watchList", arrayToJSON(getWatchList())).commit();
                }

            }
        }

        return false;
    }

    public static boolean searchOnWathcList(Movie movie) {
        if (getWatchList() != null) {
            for (int i = 0; i < getWatchList().size(); i++) {
                if (getWatchList().get(i).getId().contentEquals(movie.getId())) {
                    return true;
                }
            }
        }

        return false;
    }


    public static void adaptWatchList(ArrayList<Movie> movieArrayList) {
        if (getWatchList() != null) {
            for (int i = 0; i < movieArrayList.size(); i++) {
                Movie current = movieArrayList.get(i);
                for (int j = 0; j < getWatchList().size(); j++) {
                    if (current.getId().contentEquals(getWatchList().get(j).getId())) {
                        current.setFavorited(true);
                    }
                }
            }
        }

    }

    /*****************************************
     * FOLLOWING SECTION
     * <p>
     * ******************
     * ******************
     * ******************
     * ******************
     * ******************
     * ******************
     * ******************
     * **************************
     * **********************
     * ******************
     * **************
     * **********
     * ******
     * **
     */


    public static ArrayList<TvShow> getFollowing() {
        if (following == null) {
            following = JSONToArrayTvShow(AppController.getmSharedPrefs().getString("following", "[]"));
        }
        return following;
    }

    private static ArrayList<TvShow> JSONToArrayTvShow(String json) {
        //TODO burayı unutma
        // TODO: 16.12.2016: tamam unutmam  :D
        ArrayList<TvShow> tvShows;
        Type type = new TypeToken<ArrayList<TvShow>>() {
        }.getType();
        tvShows = AppController.getGson().fromJson(json, type);
        return tvShows;
    }

    private static String arrayToJSONTvShow(ArrayList<TvShow> arrayList) {
        String jsonArray;
        Type type = new TypeToken<ArrayList<TvShow>>() {
        }.getType();
        jsonArray = AppController.getGson().toJson(arrayList, type);
        return jsonArray;
    }


    public static boolean addToFollowing(TvShow tvShow) {
        if (getFollowing() != null) {
            getFollowing().add(tvShow);
            return AppController.getmPrefsEditor().putString("following", arrayToJSONTvShow(getFollowing())).commit();
        }
        return false;
    }

    public static boolean removeFromFollowing(TvShow tvShow) {
        if (getFollowing() != null) {
            for (int i = 0; i < getFollowing().size(); i++) {
                if (getFollowing().get(i).getId().contentEquals(tvShow.getId())) {
                    getFollowing().remove(i);
                    return AppController.getmPrefsEditor().putString("following", arrayToJSONTvShow(getFollowing())).commit();
                }

            }
        }

        return false;
    }

    public static boolean searchOnFollowing(TvShow tvShow) {
        if (getFollowing() != null) {
            for (int i = 0; i < getFollowing().size(); i++) {
                if (getFollowing().get(i).getId().contentEquals(tvShow.getId())) {
                    return true;
                }
            }
        }
        return false;
    }


    public static void adaptFollowing(ArrayList<TvShow> tvShows) {
        if (getFollowing() != null) {
            for (int i = 0; i < tvShows.size(); i++) {
                TvShow current = tvShows.get(i);
                for (int j = 0; j < getFollowing().size(); j++) {
                    if (current.getId().contentEquals(getFollowing().get(j).getId())) {
                        current.setFavorite(true);
                    }
                }
            }

        }
    }
}
