package com.alikosesakal.mooviettin.util;

import java.util.ArrayList;

/**
 * Created by ali on 16.11.2016.
 */

public class RequestGenerator {
    private static final String BACKDROP_URL = "https://image.tmdb.org/t/p/w500";
    private static final String IMAGES_URL = "https://image.tmdb.org/t/p/w300";
    private static final String MOVIES_URL = "http://api.themoviedb.org/3/movie/";
    private static final String TV_SHOWS_URL = "https://api.themoviedb.org/3/tv/";
    private static final String API_KEY = "?api_key=06ec76a2b163b06b24c66122f79d341b";
    private static final String FUNC_ON_THE_AIRING = "on_the_air";
    private static final String FUNC_TODAY = "airing_today";
    private static final String FUNC_TV_SHOW_TOP_RATED = "top_rated";
    private static final String FUNC_TV_SHOW_POPULAR = "popular";
    private static final String FUNC_UP_COMING = "upcoming";
    private static final String FUNC_NOW_PLAYING = "now_playing";
    private static final String FUNC_TOP_RATED = "top_rated";
    private static final String FUNC_POPULAR = "popular";
    private static final String THUMBNAIL_URL = "http://img.youtube.com/vi/";
    private static final String OMDb_URL = "http://www.omdbapi.com/?apikey=";
    private static final String LANGUAGE_TR = "&language=tr-TR";
    private static final String LANGUAGE_EN = "&language=en-US";
    private static final String pageTag = "&page=";


    public static String GenerateMoviesRequest(int typeID, int fragmentID, int page) {
        if (typeID == AppController.FRAGMENT_MOVIES_ID) {
            switch (fragmentID) {
                case AppController.FRAGMENT_UPCOMING:
                    return MOVIES_URL + FUNC_UP_COMING + API_KEY + LANGUAGE_EN + pageTag + page;
                case AppController.FRAGMENT_NOW_PLAYING:
                    return MOVIES_URL + FUNC_NOW_PLAYING + API_KEY + LANGUAGE_EN + pageTag + page;
                case AppController.FRAGMENT_POPULAR:
                    return MOVIES_URL + FUNC_POPULAR + API_KEY + LANGUAGE_EN + pageTag + page;
                case AppController.FRAGMENT_TOP_RATED:
                    return MOVIES_URL + FUNC_TOP_RATED + API_KEY + LANGUAGE_EN + pageTag + page;
                default:
                    return "Parameters is wrong or missing  " + typeID + " ---- " + fragmentID;
            }
        }

        return null;

    }

    public static String GenerateIMDbRequestViaImdbID(String imdbID) {
        return OMDb_URL + AppController.getOMDBApiKey() + "&i=" + imdbID;
    }

    public static String GenerateIMDbRequestViaName(String title) {
        return OMDb_URL + AppController.getOMDBApiKey() + "&t=" + title;
    }

    public static String GenerateTvShowsRequest(int typeID, int fragmentID, int page) {

        if (typeID == AppController.FRAGMENT_TV_SHOWS_ID) {
            switch (fragmentID) {
                case AppController.FRAGMENT_ON_THE_AIR:
                    return TV_SHOWS_URL + FUNC_ON_THE_AIRING + API_KEY + LANGUAGE_EN + pageTag + page;
                case AppController.FRAGMENT_TODAY:
                    return TV_SHOWS_URL + FUNC_TODAY + API_KEY + LANGUAGE_EN + pageTag + page;
                case AppController.FRAGMENT_TV_POPULAR:
                    return TV_SHOWS_URL + FUNC_TV_SHOW_POPULAR + API_KEY + LANGUAGE_EN + pageTag + page;
                case AppController.FRAGMENT_TV_TOP_RATED:
                    return TV_SHOWS_URL + FUNC_TV_SHOW_TOP_RATED + API_KEY + LANGUAGE_EN + pageTag + page;
                default:
                    return "Parameters is wrong or missing  " + typeID + " ---- " + fragmentID;
            }
        }
        return null;
    }


    public static String GenerateMovieDetails(String movieID) {
        return MOVIES_URL + movieID + API_KEY + LANGUAGE_EN;
    }

    public static String GenerateTvShowDetailsRequest(String tvShowID) {
        return TV_SHOWS_URL + tvShowID + API_KEY + LANGUAGE_EN;
    }

    public static String GenerateSimilarMovies(String movieID, int page) {
        return MOVIES_URL + movieID + "/similar" + API_KEY + LANGUAGE_EN + pageTag + page;
    }

    public static String GenerateSimilarTvShowsRequest(String tvShowID, int page) {
        return TV_SHOWS_URL + tvShowID + "/similar" + API_KEY + LANGUAGE_EN + pageTag + page;
    }

    public static ArrayList<String> GenerateYouTubeThumbnail(String key) {
        ArrayList<String> thumbnailList = new ArrayList<>();
        thumbnailList.add(THUMBNAIL_URL + key + "/0.jpg");
        thumbnailList.add(THUMBNAIL_URL + key + "/1.jpg");
        thumbnailList.add(THUMBNAIL_URL + key + "/2.jpg");
        thumbnailList.add(THUMBNAIL_URL + key + "/3.jpg");
        return thumbnailList;
    }

    public static String GenerateMovieCreditsRequest(String movieID) {
        return MOVIES_URL + movieID + "/credits" + API_KEY;
    }

    public static String GenerateTvShowCreditsRequest(String tvShowID) {
        return TV_SHOWS_URL + tvShowID + "/credits" + API_KEY;
    }

    public static String GenerateMovieVideos(String movieID) {
        return MOVIES_URL + movieID + "/videos" + API_KEY + LANGUAGE_EN;
    }

    public static String GenerateTvShowVideos(String tvShowID) {
        return TV_SHOWS_URL + tvShowID + "/videos" + API_KEY + LANGUAGE_EN;
    }


    public static String GenerateBackDropsPath(String movieID) {
        return MOVIES_URL + movieID + "/images" + API_KEY;
    }

    public static String GenerateTvShowBackDrops(String tvShowID) {
        return TV_SHOWS_URL + tvShowID + "/images" + API_KEY;
    }

    public static String GenerateBackDropImages(String imagePath) {
        return BACKDROP_URL + imagePath;
    }

    public static String GeneratePosterPath(String imagePath) {
        return IMAGES_URL + imagePath;
    }

}
