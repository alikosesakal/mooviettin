package com.alikosesakal.mooviettin.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.models.Movie;
import com.alikosesakal.mooviettin.customViews.CustomObjectCard;
import com.alikosesakal.mooviettin.models.TvShow;
import com.alikosesakal.mooviettin.util.response.AdapterBase;

import java.util.ArrayList;

/**
 * Created by ali on 18.11.2016.
 */

public class ObjectsRecyclerViewAdapter extends AdapterBase {

    private Context context;
    private ArrayList<Object> objects;
    private int typeID, fragmentID;
    private CustomObjectCard.OnItemObjectClickListener onItemClickListener;


    public ObjectsRecyclerViewAdapter(Context context, ArrayList<Object> objects,
                                      int typeID, int fragmentID, OnLoadListener onLoadListener,
                                      CustomObjectCard.OnItemObjectClickListener onItemClickListener) {
        super(objects, context, onLoadListener);
        this.context = context;
        this.objects = objects;
        this.onItemClickListener = onItemClickListener;
        this.fragmentID = fragmentID;
        this.typeID = typeID;
    }


    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyler_row_view_movie, parent, false);
        return new ObjectCardViewHolder(itemView);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindCustomViewHolder(holder, position);

        ((ObjectCardViewHolder) holder).customObjectCard.setObject(objects.get(position), onItemClickListener, typeID, fragmentID);
    }


    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof LoadingItem) {
            return TYPE_LOAD;
        }
        return 1;
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    private class ObjectCardViewHolder extends RecyclerView.ViewHolder {
        CustomObjectCard customObjectCard;

        ObjectCardViewHolder(View itemView) {
            super(itemView);
            customObjectCard = itemView.findViewById(R.id.movie_card);
        }
    }

    public int notifyMovieItem(Movie item) {
        int changedItemPosition = 0;
        for (int i = 0; i < objects.size(); i++) {
            if (((Movie) objects.get(i)).getId().equals(item.getId())) {
                objects.set(i, item);
                changedItemPosition = i;
                break;
            }
        }
        return changedItemPosition;
    }

    public int notifyTvShowItem(TvShow item) {
        int changedItemPosition = 0;
        for (int i = 0; i < objects.size(); i++) {
            if (((TvShow) objects.get(i)).getId().equals(item.getId())) {
                objects.set(i, item);
                changedItemPosition = i;
                break;
            }
        }
        return changedItemPosition;
    }

    public ArrayList<Object> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<Object> objects) {
        this.objects = objects;
    }
}
