package com.alikosesakal.mooviettin.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.customViews.CustomObjectCard;
import com.alikosesakal.mooviettin.util.response.AdapterBase;

import java.util.ArrayList;

/**
 * Created by ali on 02.12.2016.
 */

public class SimilarMoviesAdapter extends AdapterBase {
    private Context context;
    private ArrayList<Object> objects;
    private int typeID, fragmentID;
    private CustomObjectCard.OnItemObjectClickListener onItemClickListener;


    public SimilarMoviesAdapter(Context context, ArrayList<Object> objects,
                                int typeID, int fragmentID, OnLoadListener onLoadListener,
                                CustomObjectCard.OnItemObjectClickListener onItemClickListener) {
        super(objects, context, onLoadListener);
        this.context = context;
        this.objects = objects;
        this.onItemClickListener = onItemClickListener;
        this.fragmentID = fragmentID;
        this.typeID = typeID;
    }


    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row_similar_movies, parent, false);
        return new SimilarMoviesAdapter.MovieCardViewHolder(itemView);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindCustomViewHolder(holder, position);
        ((SimilarMoviesAdapter.MovieCardViewHolder) holder).customObjectCard.setObject(objects.get(position), onItemClickListener, typeID, fragmentID);
    }


    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof LoadingItem) {
            return TYPE_LOAD;
        }
        return 1;
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    public class MovieCardViewHolder extends RecyclerView.ViewHolder {
        CustomObjectCard customObjectCard;

        MovieCardViewHolder(View itemView) {
            super(itemView);
            customObjectCard = itemView.findViewById(R.id.movie_card);
        }
    }

}
