package com.alikosesakal.mooviettin.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.customViews.CustomVideoCard;
import com.alikosesakal.mooviettin.models.Video;
import com.alikosesakal.mooviettin.util.response.AdapterBase;

import java.util.ArrayList;

/**
 * Created by ali on 30.11.2016.
 */

public class TrailersRecyclerViewAdapter extends AdapterBase {
    private Context context;
    private ArrayList<Object> objects;
    private CustomVideoCard.OnItemVideoClickListener onItemClickListener;


    public TrailersRecyclerViewAdapter(Context context, ArrayList<Object> objects, OnLoadListener onLoadListener,
                                       CustomVideoCard.OnItemVideoClickListener onItemClickListener) {
        super(objects, context, onLoadListener);
        this.context = context;
        this.objects = objects;
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row_view_video_card, parent, false);
        return new TrailersRecyclerViewAdapter.VideoCardViewHolder(itemView);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindCustomViewHolder(holder, position);
        ((VideoCardViewHolder) holder).customVideoCard.setVideo((Video) objects.get(position), onItemClickListener);
    }


    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof LoadingItem) {
            return TYPE_LOAD;
        }
        return 1;
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    public class VideoCardViewHolder extends RecyclerView.ViewHolder {
        public CustomVideoCard customVideoCard;

        public VideoCardViewHolder(View itemView) {
            super(itemView);
            customVideoCard = (CustomVideoCard) itemView.findViewById(R.id.thumbnail_image_card);
        }
    }
}
