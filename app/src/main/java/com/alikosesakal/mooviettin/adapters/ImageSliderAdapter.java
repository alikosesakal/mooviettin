package com.alikosesakal.mooviettin.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.models.Image;
import com.alikosesakal.mooviettin.util.AppController;
import com.alikosesakal.mooviettin.util.RequestGenerator;

import java.util.ArrayList;

/**
 * Created by ali on 25.11.2016.
 */

public class ImageSliderAdapter extends PagerAdapter {
    private LayoutInflater mInflater;
    private ArrayList<Image> backDrops;
    String imagePath;
    private ImageView image;
    private Context mContext;


    public ImageSliderAdapter(ArrayList<Image> imagePaths) {
        this.backDrops = imagePaths;
        mContext = AppController.getContext();

    }


    @Override
    public int getCount() {
        return backDrops.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        mInflater = (LayoutInflater) AppController.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imagePath = RequestGenerator.GenerateBackDropImages(backDrops.get(position).getFilePath());
        View imageLayout = mInflater.inflate(R.layout.slider_image_view, container, false);

        image = imageLayout.findViewById(R.id.slider_image);
        Glide.with(mContext).load(imagePath).into(image);
        container.addView(imageLayout);
        return imageLayout;
    }

    public ArrayList<Image> getBackDrops() {
        return backDrops;
    }

    public void setBackDrops(ArrayList<Image> backDrops) {
        this.backDrops = backDrops;
    }
}
