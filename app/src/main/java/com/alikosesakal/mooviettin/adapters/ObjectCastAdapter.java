package com.alikosesakal.mooviettin.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.alikosesakal.mooviettin.R;
import com.alikosesakal.mooviettin.models.Cast;
import com.alikosesakal.mooviettin.util.RequestGenerator;
import com.alikosesakal.mooviettin.util.response.AdapterBase;

import java.util.ArrayList;

/**
 * Created by ali on 19.12.2016.
 */

public class ObjectCastAdapter extends AdapterBase {
    private ArrayList<Object> objects;
    private Context context;

    public ObjectCastAdapter(ArrayList<Object> objects, Context context, OnLoadListener onLoadListener) {
        super(objects, context, onLoadListener);
        this.context = context;
        this.objects = objects;
    }


    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row_cast, parent, false);
        return new CastViewHolder(itemView);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindCustomViewHolder(holder, position);
        Cast cast = (Cast) objects.get(position);
        ((CastViewHolder) holder).castName.setText(cast.getName());
        String imageUrl = RequestGenerator.GeneratePosterPath(cast.getProfilePath());
        Glide.with(context).load(imageUrl).crossFade().into(((CastViewHolder) holder).castImage);

    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof LoadingItem) {
            return TYPE_LOAD;
        }
        return 1;
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    private class CastViewHolder extends RecyclerView.ViewHolder {
        ImageView castImage;
        TextView castName;

        CastViewHolder(View itemView) {
            super(itemView);
            castImage = itemView.findViewById(R.id.cast_image);
            castName = itemView.findViewById(R.id.cast_name);
        }
    }
}
