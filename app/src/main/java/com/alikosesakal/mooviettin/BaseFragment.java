package com.alikosesakal.mooviettin;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.alikosesakal.mooviettin.util.AppController;

/**
 * Created by ali on 14.12.2016.
 */

public class BaseFragment extends Fragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AppController.getEventBus().register(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppController.getEventBus().unregister(this);
    }
}
