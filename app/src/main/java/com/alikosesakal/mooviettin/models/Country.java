package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ali on 16.11.2016.
 */

public class Country {
    @SerializedName("iso_3166_1")
    private String iso;
    @SerializedName("name")
    private String name;
}
