package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ali on 08.12.2016.
 */

public class RootTvShowJsonObject {
    @SerializedName("page")
    private String page;
    @SerializedName("results")
    private ArrayList<TvShow> results;
    @SerializedName("total_results")
    private String totalResults;
    @SerializedName("total_pages")
    private String totalPages;

    public RootTvShowJsonObject() {

    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public ArrayList<TvShow> getResults() {
        return results;
    }

    public void setResults(ArrayList<TvShow> results) {
        this.results = results;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }
}
