package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ali on 16.11.2016.
 */

public class Language {
    @SerializedName("iso_639_1")
    private String iso;
    @SerializedName("name")
    private String name;

    public Language() {

    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
