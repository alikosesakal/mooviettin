package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ali on 16.11.2016.
 */

public class Genre {
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private String id;

    public Genre() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
