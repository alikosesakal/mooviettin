package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ali on 25.11.2016.
 */

public class RootImageJsonObject {
    @SerializedName("id")
    private String id;
    @SerializedName("backdrops")
    private ArrayList<Image> backDrops;
    @SerializedName("posters")
    private ArrayList<Image> posters;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Image> getBackDrops() {
        return backDrops;
    }

    public void setBackDrops(ArrayList<Image> backDrops) {
        this.backDrops = backDrops;
    }

    public ArrayList<Image> getPosters() {
        return posters;
    }

    public void setPosters(ArrayList<Image> posters) {
        this.posters = posters;
    }
}
