package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RootVideoObject {

    @SerializedName("id")
    private String id;
    @SerializedName("results")
    private ArrayList<Video> results;

    public RootVideoObject() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Video> getResults() {
        return results;
    }

    public void setResults(ArrayList<Video> results) {
        this.results = results;
    }
}
