package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ali on 19.12.2016.
 */

public class RootCreditsObject {
    @SerializedName("id")
    private String id;
    @SerializedName("cast")
    private ArrayList<Cast> cast;
    @SerializedName("crew")
    private ArrayList<Crew> crew;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Cast> getCast() {
        return cast;
    }

    public void setCast(ArrayList<Cast> cast) {
        this.cast = cast;
    }

    public ArrayList<Crew> getCrew() {
        return crew;
    }

    public void setCrew(ArrayList<Crew> crew) {
        this.crew = crew;
    }
}
