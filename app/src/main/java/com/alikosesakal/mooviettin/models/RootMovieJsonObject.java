package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ali on 23.11.2016.
 */

public class RootMovieJsonObject {
    @SerializedName("page")
    private String page;
    @SerializedName("results")
    private ArrayList<Movie> results;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public ArrayList<Movie> getResults() {
        return results;
    }

    public void setResults(ArrayList<Movie> results) {
        this.results = results;
    }
}
