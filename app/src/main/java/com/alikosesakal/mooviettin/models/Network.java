package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ali on 08.12.2016.
 */

public class Network {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;

    public Network() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
