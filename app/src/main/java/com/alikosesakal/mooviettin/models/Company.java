package com.alikosesakal.mooviettin.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ali on 16.11.2016.
 */

public class Company {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;

    public Company() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
